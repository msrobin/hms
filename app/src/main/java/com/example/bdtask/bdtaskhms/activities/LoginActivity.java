package com.example.bdtask.bdtaskhms.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bdtask.bdtaskhms.R;
import com.example.bdtask.bdtaskhms.interfaces.HMSAPIService;
import com.example.bdtask.bdtaskhms.responses.LoginSuccessResponse;
import com.example.bdtask.bdtaskhms.retrofit.App;
import com.example.bdtask.bdtaskhms.utils.NetworkUtils;
import com.example.bdtask.bdtaskhms.utils.ProgressLoaderUtils;
import com.example.bdtask.bdtaskhms.utils.SharedPref;
import com.example.bdtask.bdtaskhms.utils.ToastUtils;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @BindView(R.id.met_Email)
    MaterialEditText metEmail;

    @BindView(R.id.met_Password)
    MaterialEditText metPassword;

    @BindView(R.id.tv_Reg)
    TextView tvReg;

    @BindView(R.id.btn_Login)
    Button btnLogin;

    private HMSAPIService hmsapiService;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SharedPref.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hmsapiService = retrofit.create(HMSAPIService.class);


//        hmsapiService = ((App)getApplication()).getHmsapiService();

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);


//        moveToRegistrationActivity();

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor("#000000"));


            }

    @OnClick(R.id.tv_Reg)
        void moveToRegistrationActivity(){
            Intent intent=new Intent(LoginActivity.this,RegistrationActivity.class);
            startActivity(intent);
    }




    @OnClick(R.id.btn_Login)
    void onLogin(){

        String email = metEmail.getText().toString().trim();
        String password = metPassword.getText().toString();

        if(TextUtils.isEmpty(email)){
            ToastUtils.toast(getApplicationContext(), getString(R.string.alert_enter_email));
            return;
        }
        if(TextUtils.isEmpty(password)){
            ToastUtils.toast(getApplicationContext(), getString(R.string.alert_enter_password));
            return;
        }

        if((TextUtils.isEmpty(email))&&(TextUtils.isEmpty(password))) {
            ToastUtils.toast(getApplicationContext(), getString(R.string.alert_enter_email_and_password));
            return;
        }

        if (!email.matches(EMAIL_PATTERN)){
            ToastUtils.toast(getApplicationContext(), getString(R.string.wrong_email_format));
            return;
        }

        if(NetworkUtils.isNetworkConnected(this)){

            ProgressLoaderUtils.showProgressDialog(LoginActivity.this, getString(R.string.title_login), getString(R.string.msg_login));


            hmsapiService.doLogin(email, password)
                    .enqueue(new Callback<LoginSuccessResponse>() {
                        @Override
                        public void onResponse(Call<LoginSuccessResponse> call, Response<LoginSuccessResponse> response) {
                            ProgressLoaderUtils.dismissProgressDialog();
                            Log.d(TAG, "onResponse: ");
                            try{
                                LoginSuccessResponse loginResponse = response.body();
                                if(loginResponse.getResponse().getStatus().equals("ok")){
                                    Gson gson = new Gson();
                                    String loginResponseObjectText = gson.toJson(loginResponse);
                                    SharedPref.saveToPrefs(LoginActivity.this, SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_EMAIL, email);
                                    SharedPref.saveToPrefs(LoginActivity.this, SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_PASSWORD, password);
//                                    String email=loginResponse.getResponse().
//                                    SharedPref.write(Constant.PREF_IS_LOGGED_IN, true);
//                                    SharedPref.write(Constant.PREF_LOGIN_RESPONSE_OBJ, loginResponseObjectText);
                                    SharedPref.saveToPrefs(LoginActivity.this, SharedPref.SHARED_PREF_MAIN, SharedPref.PREF_LOGIN_RESPONSE_OBJ, loginResponseObjectText);
                                    SharedPref.saveToPrefs(LoginActivity.this, SharedPref.SHARED_PREF_MAIN, SharedPref.USER_LOGIN_SESSION, SharedPref.STATUS_TRUE);
                                    moveToMainActivity();

                                } else{
                                    ToastUtils.toast(getApplicationContext(), getString(R.string.alert_fail_to_login));
                                      }

                            } catch (Exception e){
                                Log.d(TAG, "onResponse: "+e.getLocalizedMessage());
                            }
                        }
                        @Override
                        public void onFailure(Call<LoginSuccessResponse> call, Throwable t) {
                            ProgressLoaderUtils.dismissProgressDialog();
                            Log.d(TAG, "onFailure: "+t.getLocalizedMessage());
                        }
                    });
        } else{
            showInternetNotAvailableDialog();
        }
    }







    private void showInternetNotAvailableDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        final String action = Settings.ACTION_WIFI_SETTINGS;
        final String title = getString(R.string.title_no_network);
        final String message = getString(R.string.msg_no_network_available);

        builder.setMessage(message)
                .setTitle(title)
                .setPositiveButton(R.string.ok,
                        (d, id) -> {
                            startActivity(new Intent(action));
                            d.dismiss();
                        })
                .setNegativeButton("Cancel",//here cancel need to take in string value
                        (d, id) ->
                         d.cancel());
        builder.create().show();
    }

    private void moveToMainActivity()

    {
        Intent i= new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
        finish();
    }
}
