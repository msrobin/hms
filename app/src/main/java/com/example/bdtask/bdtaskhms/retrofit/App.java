package com.example.bdtask.bdtaskhms.retrofit;

import android.app.Application;

import com.example.bdtask.bdtaskhms.interfaces.HMSAPIService;
import com.squareup.picasso.Picasso;


public class App extends Application {


    private static Picasso picasso;
    private HMSAPIService hmsapiService;
    @Override
    public void onCreate() {
        super.onCreate();
        hmsapiService = ApiClient.getClient().create(HMSAPIService.class);
    }
    public HMSAPIService getHmsapiService() {
        return hmsapiService;
    }

    public Picasso getPicasso()
    {
        picasso = new Picasso.Builder(getApplicationContext()).build();
        return picasso;

    }

}
