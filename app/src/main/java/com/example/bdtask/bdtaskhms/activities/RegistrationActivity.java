package com.example.bdtask.bdtaskhms.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bdtask.bdtaskhms.R;
import com.example.bdtask.bdtaskhms.interfaces.HMSAPIService;
import com.example.bdtask.bdtaskhms.models.LoginResponse;
import com.example.bdtask.bdtaskhms.models.PatientInfo;
import com.example.bdtask.bdtaskhms.responses.LoginSuccessResponse;
import com.example.bdtask.bdtaskhms.responses.RegSuccessResponse;
import com.example.bdtask.bdtaskhms.utils.LocaleHelper;
import com.example.bdtask.bdtaskhms.utils.NetworkUtils;
import com.example.bdtask.bdtaskhms.utils.ProgressLoaderUtils;
import com.example.bdtask.bdtaskhms.utils.SharedPref;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static com.example.bdtask.bdtaskhms.utils.SharedPref.LANGUAGE;
import static com.example.bdtask.bdtaskhms.utils.SharedPref.getFromPrefs;

public class RegistrationActivity extends AppCompatActivity  {

    private static final String TAG = "RegistrationActivity";
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;


    @BindView(R.id.met_first_name)
    MaterialEditText metFirstName;

    @BindView(R.id.met_last_name)
    MaterialEditText metLastName;

    @BindView(R.id.met_reg_email)
    MaterialEditText metregEmail;

    @BindView(R.id.met_reg_password)
    MaterialEditText metRegPassword;

    @BindView(R.id.met_phone_number)
    MaterialEditText metPhoneNumber;

    @BindView(R.id.met_mobile_number)
    MaterialEditText metMobileNumber;

    @BindView(R.id.sp_blood_group)
    Spinner spBloodGroup;

    @BindView(R.id.sp_gender)
    Spinner spGender;

    @BindView(R.id.met_dob)
    MaterialEditText metDob;

    @BindView(R.id.btn_select_pic)
    Button btnSelectPic;

    @BindView(R.id.tv_pic_name)
    TextView tvPicName;

    @BindView(R.id.met_address)
    MaterialEditText metAddress;

    @BindView(R.id.btn_save)
    Button btnSave;

    private ArrayList<PatientInfo> patientInfos = new ArrayList<>();
    PatientInfo patientInfo;
    private final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String base_url;
    static SpotsDialog progressDialog;
    public Retrofit retrofit;
    private HMSAPIService hmsapiService;
    String gender;
    String language;
    Resources resources;
    RegSuccessResponse regSuccessResponse;
    LoginSuccessResponse loginSuccessResponse;
    LoginResponse loginResponse;

    File destination;
    String imagePath;
    private static final int REQUEST_IMAGE = 100;

    private String imagePathUrl;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        language = getFromPrefs(newBase, SharedPref.SHARED_PREF_MAIN, LANGUAGE);
//        super.attachBaseContext(LocaleHelper.onAttach(newBase, language));
//    }
//
//    private void updateViews(String languageCode) {
//        Context context = LocaleHelper.setLocale(this, languageCode);
//        resources = context.getResources();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        base_url = getFromPrefs(RegistrationActivity.this, SharedPref.SHARED_PREF_MAIN, SharedPref.BASE_URL);
        progressDialog = new SpotsDialog(RegistrationActivity.this, R.style.Custom);
        patientInfo= new PatientInfo();

        regSuccessResponse = new RegSuccessResponse();

//        retrofit = new Retrofit.Builder()
//                .baseUrl(SharedPref.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        hmsapiService = retrofit.create(HMSAPIService.class);



        ArrayAdapter<CharSequence> bg_adapter = ArrayAdapter.createFromResource(this, R.array.blood_group, android.R.layout.simple_spinner_item);
        bg_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBloodGroup.setAdapter(bg_adapter);
        ArrayAdapter<CharSequence> gender_adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_item);
        gender_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(gender_adapter);




        btnSelectPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        metDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        int s=monthOfYear+1;
                        String a = dayOfMonth+"/"+s+"/"+year;
                        metDob.setText(""+a);
                    }
                };
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog d = new DatePickerDialog(RegistrationActivity.this,R.style.DialogThemeOne, dpd, year ,month, day);
                d.show();

            }
        });
    }





    @OnClick(R.id.btn_save)
    void onSubmitParticipantInfo() {


        String firstName = metFirstName.getText().toString().trim();
        String lastName = metLastName.getText().toString().trim();
        String email = metregEmail.getText().toString().trim();
        String password = metRegPassword.getText().toString().trim();
        String phoneNumber = metPhoneNumber.getText().toString().trim();
        String mobileNumber = metMobileNumber.getText().toString().trim();
        String bloodGroup = spBloodGroup.getSelectedItem().toString().trim();
        String gender = spGender.getSelectedItem().toString().trim();
        String dob = metDob.getText().toString().trim();
        String path=tvPicName.getText().toString().trim();
        String address = metAddress.getText().toString().trim();



        patientInfo.setFirstname(firstName);
        patientInfo.setLastname(lastName);
        patientInfo.setEmail(email);
        patientInfo.setPassword(password);
        patientInfo.setPhone(phoneNumber);
        patientInfo.setMobile(mobileNumber);
        patientInfo.setBloodGroup(bloodGroup);
        patientInfo.setSex(gender);
        patientInfo.setDateOfBirth(dob);
        patientInfo.setPicture(path);
        patientInfo.setAddress(address);


//        patientInfo.getStatus();

        if(regSuccessResponse.getResponse()!=null){
            if(regSuccessResponse.getResponse().getPatientInfoList()!=null){
                regSuccessResponse.getResponse().getPatientInfoList().add(patientInfo);
            }
        }
//        patientInfos.add(patientInfo);

//regSuccessResponse.getResponse().getStatus().
        Gson gson = new Gson();
        String patientInfoobjectstring = gson.toJson(patientInfo);
        Log.e(TAG, patientInfoobjectstring);
//        if(NetworkUtils.isNetworkConnected(this)){
//
//            ProgressLoaderUtils.showProgressDialog(RegistrationActivity.this, getString(R.string.title_login), getString(R.string.msg_login));
//
//
//            hmsapiService.doLogin(email, password)
//                    .enqueue(new Callback<Response>() {
//                        @Override
//                        public void onResponse(Call<Response> call, Response<Response> response) {
//                            ProgressLoaderUtils.dismissProgressDialog();
//                            Log.d(TAG, "onResponse: ");
//                            try{
//                                LoginSuccessResponse loginResponse = response.body();
//                                if(loginResponse.getResponse().getStatus().equals("ok")){
//                                    Gson gson = new Gson();
//                                    String loginResponseObjectText = gson.toJson(loginResponse);












//        patientInfo=new PatientInfo();
////        patientInfo.setFirstname(firstName);
//        Log.d(TAG, firstName+" "+bloodGroup+" "+gender);
//        patientInfos.add(0,patientInfo.setFirstname(firstName));
//        for (PatientInfo patientInfo : patientInfos) {
//            Arrays.stream(patientInfo)
//                    .forEach(patientInfos::add);
//        }


//        Calendar calendar = Calendar.getInstance();
//        String id = surveyDatum.getSurveyHeaderId() + "" + calendar.getTimeInMillis();
//        String name = etParticipantName.getText().toString().trim();
//        String email = etParticipantEmail.getText().toString().trim();
//        String phone = etParticipantPhone.getText().toString().trim();
//        String address = etParticipantAddress.getText().toString().trim();
//
//        if(TextUtils.isEmpty(name)){
//            toast("Please participant name");
//            return;
//        }
//
//        if(TextUtils.isEmpty(email)){
//            toast("Please participant email");
//            return;
//        }
//
//        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
//            toast("Please enter valid email address");
//            return;
//        }
//
//        if(TextUtils.isEmpty(phone)){
//            toast("Please participant phone");
//            return;
//        }
//
//        if(TextUtils.isEmpty(address)){
//            toast("Please participant address");
//            return;
//        }
//
//
//        suplSurvey.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
//
//        participant = new Participant();
//        participant.setParticipantId(id);
//        participant.setParticipantName(name);
//        participant.setParticipantEmail(email);
//        participant.setParticipantPhone(phone);
//        participant.setParticipantAddress(address);
//        participant.setParticipantRequired(1);
        retrofit = new Retrofit.Builder()
                .baseUrl(SharedPref.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hmsapiService = retrofit.create(HMSAPIService.class);

        Call<RegSuccessResponse> call = hmsapiService.doReg(patientInfo);
        call.enqueue(new Callback<RegSuccessResponse>() {
            @Override
            public void onResponse(Call<RegSuccessResponse> call, Response<RegSuccessResponse> response) {

                if(response.body().getResponse().getStatus().equals("ok"))
                {
                    regSuccessResponse = response.body();
                    Toast.makeText(RegistrationActivity.this, "Registration successful!!!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegistrationActivity.this, MainActivity.class).putExtra("go_to", "patient_info"));
                    progressDialog.dismiss();
//                    Log.d(TAG, regSuccessResponse.getResponse().getMessage());
                    finish();
                }
            }

            @Override
            public void onFailure(Call<RegSuccessResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: ");

            }
        });


        }



    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle(getString(R.string.select_action));
        String[] pictureDialogItems = {
                getString(R.string.select_picture_from_gallery),
                getString(R.string.capture_picture_from_camera) };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(RegistrationActivity.this, getString(R.string.image_saved), Toast.LENGTH_SHORT).show();
//                    imageview.setImageBitmap(bitmap);
                    tvPicName.setText(path);
                    Log.d(TAG, path);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(RegistrationActivity.this, getString(R.string.failed), Toast.LENGTH_SHORT).show();

                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//            imageview.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            String path = saveImage(thumbnail);
            tvPicName.setText(path);
            Log.d(TAG, path);
            Toast.makeText(RegistrationActivity.this, R.string.image_saved, Toast.LENGTH_SHORT).show();
        }
    }






    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", R.string.image_saved + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }





}
