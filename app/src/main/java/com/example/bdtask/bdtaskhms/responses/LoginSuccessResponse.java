package com.example.bdtask.bdtaskhms.responses;

import com.example.bdtask.bdtaskhms.models.LoginResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginSuccessResponse implements Serializable {

    @SerializedName("response")
    @Expose
    private LoginResponse response;

    public LoginResponse getResponse() {
        return response;
    }

    public void setResponse(LoginResponse response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "LoginSuccessResponse{" +
                "response=" + response +
                '}';
    }
}
