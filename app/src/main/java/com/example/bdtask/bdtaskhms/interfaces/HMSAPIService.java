package com.example.bdtask.bdtaskhms.interfaces;

import com.example.bdtask.bdtaskhms.models.PatientInfo;
import com.example.bdtask.bdtaskhms.responses.LoginSuccessResponse;
import com.example.bdtask.bdtaskhms.responses.RegSuccessResponse;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface HMSAPIService {

    @GET("auth/login")
    Call<LoginSuccessResponse> doLogin(@Query("email") String email, @Query("password") String password);

//    @POST("auth/registrartion")
//    Call<LoginSuccessResponse> doReg(@Body("email") String email, @Query("password") String password);

    @POST("auth/registration")
    Call<RegSuccessResponse> doReg(@Body PatientInfo patientInfo);

}
