package com.example.bdtask.bdtaskhms.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LoginResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("user_data")
    @Expose
    private PatientInfo patientInfo;

    public List<PatientInfo> getPatientInfos() {
        return patientInfos;
    }

    public void setPatientInfos(List<PatientInfo> patientInfos) {
        this.patientInfos = patientInfos;
    }

    private List<PatientInfo> patientInfos;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PatientInfo getPatientInfo() {
        return patientInfo;
    }

    public void setPatientInfo(PatientInfo patientInfo) {
        this.patientInfo = patientInfo;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", patientInfo=" + patientInfo +
                '}';
    }
}
