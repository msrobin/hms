package com.example.bdtask.bdtaskhms.models;

import com.example.bdtask.bdtaskhms.models.PatientInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RegResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    private List<PatientInfo> patientInfoList;

    public List<PatientInfo> getPatientInfoList() {
        return patientInfoList;
    }

    public void setPatientInfoList(List<PatientInfo> patientInfoList) {
        this.patientInfoList = patientInfoList;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RegResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", patientInfoList=" + patientInfoList +
                '}';
    }
}

