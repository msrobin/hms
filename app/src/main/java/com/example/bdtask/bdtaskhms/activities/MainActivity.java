package com.example.bdtask.bdtaskhms.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


import com.example.bdtask.bdtaskhms.R;
import com.example.bdtask.bdtaskhms.fragments.AppointmentFragment;
import com.example.bdtask.bdtaskhms.responses.LoginSuccessResponse;
import com.example.bdtask.bdtaskhms.retrofit.App;
import com.example.bdtask.bdtaskhms.utils.SharedPref;
import com.google.gson.Gson;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AppointmentFragment.OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;


//    @BindView((R.id.civ_profile_pic))
//    CircleImageView civProfilePic;

    private FragmentManager manager;
    private LoginSuccessResponse loginSuccessResponse;

    private Picasso picasso;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        SharedPref.init(MainActivity.this);

        String loginResponseObjectText = SharedPref.getFromPrefs(this,SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_LOGIN_RESPONSE_OBJ);
        Gson gson = new Gson();
        loginSuccessResponse = gson.fromJson(loginResponseObjectText, LoginSuccessResponse.class);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        picasso = ((App) getApplication()).getPicasso();



        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        manager = getSupportFragmentManager();

        setupHeaderView();
    }

    private void setupHeaderView() {

        View headerView = navigationView.getHeaderView(0);
        CircleImageView civProfilePic = headerView.findViewById(R.id.civ_profile_pic);
        TextView tvProfileName = headerView.findViewById(R.id.tv_profile_name);
        TextView tvProfileEmail = headerView.findViewById(R.id.tv_profile_email);

        if(loginSuccessResponse != null){
            String url = loginSuccessResponse.getResponse().getPatientInfo().getPicture();
            url = SharedPref.BASE_URL_ + url;
            getPicasso().with(MainActivity.this)
                    .load(url)
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user)
                    .into(civProfilePic);
            tvProfileName.setText(loginSuccessResponse.getResponse().getPatientInfo().getFirstname());
            tvProfileEmail.setText(loginSuccessResponse.getResponse().getPatientInfo().getEmail());
            Log.d(TAG, url);
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        setupFragmentView(item);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupFragmentView(MenuItem item) {

        if(item.getItemId() == R.id.edit_profile){




        }
        else if(item.getItemId() == R.id.appointment){
            AppointmentFragment fragment = new AppointmentFragment();
            manager.beginTransaction().replace(R.id.content_main, fragment, fragment.getTag())
                    .commit();

        } else if(item.getItemId() == R.id.download_documnts){
}
         else if(item.getItemId() == R.id.upload_documents){

        }
        else if(item.getItemId() == R.id.log_out){


            //Log out task
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(R.string.logout_confirmation_message);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    SharedPreferences mPreferences = getSharedPreferences(SharedPref.SHARED_PREF_MAIN, MODE_PRIVATE);
                    SharedPreferences.Editor editor = mPreferences.edit();
                    editor.remove(SharedPref.PREF_LOGIN_RESPONSE_OBJ);
                    editor.remove(SharedPref.PREF_EMAIL);
                    editor.remove(SharedPref.PREF_PASSWORD);
                    editor.commit();
                    editor.clear();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }
            });

            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            AlertDialog alert=builder.create();
            alert.show();

        }
    }

    public Picasso getPicasso()
    {
        picasso = new Picasso.Builder(getApplicationContext()).build();
        return picasso;

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
