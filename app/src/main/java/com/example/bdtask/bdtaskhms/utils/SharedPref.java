package com.example.bdtask.bdtaskhms.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

public class SharedPref {

    public static final String SHARED_PREF_MAIN="shared_preference_main";


    public static final String CAT_ITEM_COUNT="cat_item_count";
    public static final String STATUS_TRUE="true";
    public static final String STATUS_FALSE="false";


    public static final String BASE_URL="http://soft12.bdtask.com/hospital_v6.1/api/";
    public static final String BASE_URL_="http://soft12.bdtask.com/hospital_v6.1/";
    public static final String ISCONNECTED="is_connected";

    public static final String USER_ID="user_id";
    public static final String USER_LOGIN_SESSION="user_login_session";

    public static final String LANGUAGE="Language";
    public static final String PREF_LOGIN_RESPONSE_OBJ ="login_response_object" ;
    public static final String PREF_EMAIL ="pref_email" ;
    public static final String PREF_PASSWORD = "pref_password";


    public static void saveToPrefs(Context context, String prefsname, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(prefsname,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getFromPrefs(Context context,String prefsname, String key) {
        if(context!=null){
            SharedPreferences prefs = context.getSharedPreferences(prefsname,context.MODE_PRIVATE);
            try {
                return prefs.getString(key, "null");
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                return "null";
            }
        }
        return "null";

    }

}
