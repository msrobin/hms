package com.example.bdtask.bdtaskhms.utils;

import android.app.Activity;
import android.app.ProgressDialog;

public final class ProgressLoaderUtils {

    private static ProgressDialog progressDialog;

    public static void showProgressDialog(Activity activity, String title, String message){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void dismissProgressDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

}
