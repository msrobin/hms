package com.example.bdtask.bdtaskhms.responses;

import com.example.bdtask.bdtaskhms.models.LoginResponse;
import com.example.bdtask.bdtaskhms.models.RegResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegSuccessResponse {
    @SerializedName("response")
    @Expose
    private RegResponse response;

    public RegResponse getResponse() {
        return response;
    }
    public void setResponse(RegResponse response) {
        this.response = response;

    }
    @Override
    public String toString() {
        return "RegSuccessResponse{" +
                "response=" + response +
                '}';
    }
}
