package com.example.bdtask.bdtaskhms.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.example.bdtask.bdtaskhms.R;
import com.example.bdtask.bdtaskhms.interfaces.HMSAPIService;
import com.example.bdtask.bdtaskhms.responses.LoginSuccessResponse;
import com.example.bdtask.bdtaskhms.utils.LocaleHelper;
import com.example.bdtask.bdtaskhms.utils.NetworkUtils;
import com.example.bdtask.bdtaskhms.utils.SharedPref;
import com.google.gson.Gson;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.bdtask.bdtaskhms.utils.SharedPref.LANGUAGE;


public class Splash extends AppCompatActivity {

    private static final String TAG = "Splash";
    private HMSAPIService hmsapiService;

    @BindView(R.id.powerd_by_title)
    TextView poweredByTitle;


    private ConstraintLayout constraintLayout;
    String language;
    Resources resources;
    @Override
    protected void attachBaseContext(Context newBase) {
        language=SharedPref.getFromPrefs(newBase,SharedPref.SHARED_PREF_MAIN,LANGUAGE);
        super.attachBaseContext(LocaleHelper.onAttach(newBase,language));
    }
    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        resources = context.getResources();
    }
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor("#000000"));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SharedPref.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hmsapiService = retrofit.create(HMSAPIService.class);

        if(language!=null){
            updateViews(language);
        }
        constraintLayout= (ConstraintLayout) findViewById(R.id.flash_screen_layout);

        Animation fadeout = AnimationUtils.loadAnimation(Splash.this, R.anim.fade_out);

        poweredByTitle.setText(resources.getString(R.string.powered_by));

        constraintLayout.startAnimation(fadeout);
        fadeout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {


                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        /* Create an Intent that will start the Menu-Activity. */


//                        if(SharedPref.getFromPrefs(Splash.this,SharedPref.SHARED_PREF_MAIN,SharedPref.ISCONNECTED).equals(SharedPref.STATUS_TRUE)){
//
//                            if(SharedPref.getFromPrefs(Splash.this,SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_LOGIN_RESPONSE_OBJ).equals(SharedPref.STATUS_TRUE)){
//                                Intent i = new Intent(Splash.this,MainActivity.class);
//                                startActivity(i);
//                            }
//                            else {
//                                Intent i = new Intent(Splash.this,MainActivity.class);
//                                startActivity(i);
//                            }
//                        }
//                        else
//                            {
//                            Intent mainIntent = new Intent(Splash.this,LoginActivity.class);
//                            startActivity(mainIntent);
//                        }
//
//                        finish();

                        if((SharedPref.getFromPrefs(Splash.this,SharedPref.SHARED_PREF_MAIN,SharedPref.USER_LOGIN_SESSION).equals(SharedPref.STATUS_TRUE))){
                            String email = SharedPref.getFromPrefs(Splash.this,SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_EMAIL);
                            String password = SharedPref.getFromPrefs(Splash.this,SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_PASSWORD);

                            if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
                                if(NetworkUtils.isNetworkConnected(Splash.this)){
                                    doLogin(email, password);
                                } else{
                                    moveToMainActivity();
                                }

                            } else{
                               moveToLoginActivity();
                            }
                        } else{
                            moveToLoginActivity();
                        }
                        }
                }, 1800);
                Animation fadeIn = AnimationUtils.loadAnimation(Splash.this, R.anim.fade_in);
                constraintLayout.startAnimation(fadeIn);



            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }



    private void doLogin(final String email, final String password){
        hmsapiService.doLogin(email, password)
                .enqueue(new Callback<LoginSuccessResponse>() {
                    @Override
                    public void onResponse(Call<LoginSuccessResponse> call, Response<LoginSuccessResponse> response) {
                        try{
                            LoginSuccessResponse loginResponse = response.body();
                            if(loginResponse.getResponse().getStatus().equals("ok")){
                                Gson gson = new Gson();
                                String loginResponseObjectText = gson.toJson(loginResponse);
                                SharedPref.saveToPrefs(Splash.this, SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_EMAIL, email);
                                SharedPref.saveToPrefs(Splash.this, SharedPref.SHARED_PREF_MAIN,SharedPref.PREF_PASSWORD, password);
                                SharedPref.saveToPrefs(Splash.this, SharedPref.SHARED_PREF_MAIN, SharedPref.PREF_LOGIN_RESPONSE_OBJ, loginResponseObjectText);
                                SharedPref.saveToPrefs(Splash.this, SharedPref.SHARED_PREF_MAIN, SharedPref.USER_LOGIN_SESSION, SharedPref.STATUS_TRUE);
                                moveToMainActivity();
                            } else{
                                moveToLoginActivity();
                            }

                        } catch (Exception e){
                            Log.d(TAG, "onResponse: "+e.getLocalizedMessage());
                            moveToLoginActivity();
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginSuccessResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure: "+t.getLocalizedMessage());
                        moveToLoginActivity();
                    }
                });
    }

    private void moveToMainActivity() {
        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainIntent);
        finish();
    }

    private void moveToLoginActivity() {
        Intent mainIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(mainIntent);
        finish();
    }



}
