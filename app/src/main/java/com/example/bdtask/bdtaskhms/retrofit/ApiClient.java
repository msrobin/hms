package com.example.bdtask.bdtaskhms.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.bdtask.bdtaskhms.utils.SharedPref.BASE_URL;


public class ApiClient {
    private static Retrofit retrofit;
    static Retrofit getClient()
    {
        if(retrofit==null)
        {
            Gson gson= new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit= new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }



}
